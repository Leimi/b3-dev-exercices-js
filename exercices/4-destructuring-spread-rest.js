/* npm test exercices/4-destructuring-spread-rest.js

Instructions :
- Changez le code pour utiliser du destructuring et des opérateurs "spread"/"rest" et faire que les tests passent
- ne changez pas les lignes `assert...`
*/
import test from 'ava';
import { strict as assert } from 'assert';

test('Destructuring', t => {
	const Guile = {
		firstname: 'Jean Claude',
		lastname: 'Van Damme',
	};

	const lostNumbers = [4, 8, 15, 16, 23, 42]
	
	// DÉBUT : utilisez du destructuring ici pour créer "firstname" et "lastname"
	// FIN

	assert.strictEqual(firstname, 'Jean Claude');
	assert.strictEqual(lastname, 'Van Damme');

	// DÉBUT : utilisez du destructuring ici pour créer "firstNumber", "secondNumber", "otherNumbers"
	// FIN

	assert.strictEqual(firstNumber, 4);
	assert.strictEqual(secondNumber, 8);
	assert.deepStrictEqual(otherNumbers, [15, 16, 23, 42]);
});

test('Spread', t => {
	const chienMignon = {
		mignon: true,
		age: 4,
	}

	// DÉBUT : utilisez l'opérateur spread ici pour faire passer les tests
	const autreChien = chienMignon
	// FIN

	assert.notStrictEqual(autreChien, chienMignon)

	const chienMignonEtTalentueux = {
		mignon: true,
		age: 5,
		talents: ['la baballe', 'les belly hugs']
	}

	// DÉBUT : utilisez l'opérateur spread ici pour faire passer les tests
	const autreChienTalentueux = chienMignonEtTalentueux
	// FIN

	assert.notStrictEqual(autreChienTalentueux, chienMignonEtTalentueux)
	assert.notStrictEqual(autreChienTalentueux.talents, chienMignonEtTalentueux.talents)

})

test('Rest', t => {
	// implémentez cette fonction pour que les tests passent
	function bonjourAJeSaisPasCombienDeGens() {
	}

	assert.strictEqual(
		bonjourAJeSaisPasCombienDeGens('Jean'),
		'Salut Jean !'
	)

	assert.strictEqual(
		bonjourAJeSaisPasCombienDeGens('Jean', 'Claude'),
		'Salut Jean ! Salut Claude !'
	)

	assert.strictEqual(
		bonjourAJeSaisPasCombienDeGens('Jean', 'Claude', 'Pierre'),
		'Salut Jean ! Salut Claude ! Salut Pierre !'
	)

	assert.strictEqual(
		bonjourAJeSaisPasCombienDeGens('Marie', 'Jacques', 'Annelyse', 'Coline', 'François', 'Aurélie', 'Moundir', 'Martin'),
		'Salut Marie ! Salut Jacques ! Salut Annelyse ! Salut Coline ! Salut François ! Salut Aurélie ! Salut Moundir ! Salut Martin !'
	)
})